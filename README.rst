=========================
The ``lino-cosi`` package
=========================





- User Guide:
  https://using.lino-framework.org/apps/cosi

- Source code:
  https://gitlab.com/lino-framework/cosi

- Changelog: https://dev.lino-framework.org/changes

- Public demo site:
  https://cosi1e.lino-framework.org

- Developer documentation:
  https://www.lino-framework.org/apps/cosi

- Lino Così is an integral part of the Lino framework, which is documented
  at https://www.lino-framework.org

- Lino Così is a sustainably free open-source project. Your contributions are
  welcome.  See https://community.lino-framework.org for details.

- Professional hosting, support and maintenance:
  https://www.saffre-rumma.net

- Legacy docs about Lino Così:
  https://cosi.lino-framework.org
  https://cosi.lino-framework.org/de

- Author: https://www.synodalsoft.net
