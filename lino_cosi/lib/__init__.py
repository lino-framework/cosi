# -*- coding: UTF-8 -*-
# Copyright 2013-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""

Plugins
=======

.. autosummary::
   :toctree:

    cosi
    contacts
    products


"""
