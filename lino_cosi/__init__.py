# -*- coding: UTF-8 -*-
# Copyright 2013-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
.. autosummary::
   :toctree:

    lib
    migrate

"""

__version__ = '25.1.0'

intersphinx_urls = dict(docs="https://cosi.lino-framework.org")


ATELIER_INFO = dict(
    nickname="cosi",
    verbose_name="Lino Così",
    # srcref_url='https://gitlab.com/lino-framework/cosi/blob/master/%s',
    # intersphinx_urls=dict(docs="https://cosi.lino-framework.org"),
)
